use std::fmt;

// 0 1 2 3 4 5 6 7
// c j z w f s b v

pub struct HexV2(String);

impl HexV2 {
    pub fn from_lower_hex(s: &str) -> Self {
        let mut buf = String::with_capacity(s.len());
        s.chars().for_each(|c| {
            buf.push(match c {
                '0' => '0',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => 'c',
                '9' => 'j',
                'a' => 'z',
                'b' => 'w',
                'c' => 'f',
                'd' => 's',
                'e' => 'b',
                'f' => 'v',
                _ => panic!("\"{c}\" is not valid lower hex string"),
            });
        });
        Self(buf)
    }

    pub fn from_upper_hex(s: &str) -> Self {
        let mut buf = String::with_capacity(s.len());
        s.chars().for_each(|c| {
            buf.push(match c {
                '0' => '0',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => 'c',
                '9' => 'j',
                'A' => 'z',
                'B' => 'w',
                'C' => 'f',
                'D' => 's',
                'E' => 'b',
                'F' => 'v',
                _ => panic!("\"{c}\" is not valid upper hex string"),
            });
        });
        Self(buf)
    }
}

impl fmt::Display for HexV2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.pad_integral(true, "", &self.0)
    }
}

impl fmt::LowerHex for HexV2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut buf = String::with_capacity(self.0.len());
        self.0.chars().for_each(|c| {
            buf.push(match c {
                '0' => '0',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                'c' => '8',
                'j' => '9',
                'z' => 'a',
                'w' => 'b',
                'f' => 'c',
                's' => 'd',
                'b' => 'e',
                'v' => 'f',
                _ => unreachable!(""),
            });
        });
        f.pad_integral(true, "", &buf)
    }
}

impl fmt::UpperHex for HexV2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut buf = String::with_capacity(self.0.len());
        self.0.chars().for_each(|c| {
            buf.push(match c {
                '0' => '0',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                'c' => '8',
                'j' => '9',
                'z' => 'A',
                'w' => 'B',
                'f' => 'C',
                's' => 'D',
                'b' => 'E',
                'v' => 'F',
                _ => unreachable!(""),
            });
        });
        f.pad_integral(true, "", &buf)
    }
}

#[cfg(test)]
mod tests {
    use super::HexV2;

    #[test]
    fn from_valid_lower_hex() {
        let v2 = HexV2::from_lower_hex("0123456789abcdef");
        assert_eq!(v2.0, "01234567cjzwfsbv".to_owned());
    }

    #[test]
    #[should_panic(expected = "\"i\" is not valid lower hex string")]
    fn from_invalid_lower_hex() {
        HexV2::from_lower_hex("invalid");
    }

    #[test]
    fn from_upper_hex() {
        let v2 = HexV2::from_upper_hex("0123456789ABCDEF");
        assert_eq!(v2.0, "01234567cjzwfsbv".to_owned());
    }

    #[test]
    #[should_panic(expected = "\"I\" is not valid upper hex string")]
    fn from_invalid_upper_hex() {
        HexV2::from_upper_hex("INVALID");
    }

    #[test]
    fn display() {
        let v2 = HexV2::from_lower_hex("0123456789abcdef");
        assert_eq!(format!("{v2}"), "01234567cjzwfsbv");
    }

    #[test]
    fn to_lower_hex() {
        let v2 = HexV2::from_lower_hex("0123456789abcdef");
        assert_eq!(format!("{:x}", v2), "0123456789abcdef");
    }

    #[test]
    fn to_upper_hex() {
        let v2 = HexV2::from_lower_hex("0123456789abcdef");
        assert_eq!(format!("{:X}", v2), "0123456789ABCDEF");
    }
}
