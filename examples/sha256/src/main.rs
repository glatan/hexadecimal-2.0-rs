use ghash::{Hash, Sha256};
use hex_v2::HexV2;

fn main() {
    let digest = Sha256::default().hash_to_lowerhex(&[]);
    println!("LowerHex: {}", digest);
    println!("RFC 9226: {}", HexV2::from_lower_hex(&digest));
}
